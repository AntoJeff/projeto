﻿namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    public class PedidoItemDTO
    { 
        public int Id { get; set;} 
        public int IdPedido { get; set;} 
        public int IdProduto { get; set; } 

    }
}