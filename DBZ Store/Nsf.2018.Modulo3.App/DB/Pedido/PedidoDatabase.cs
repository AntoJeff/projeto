﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDatabase
    { 
        public int Salvar(PedidoDTO dto)
        {
            string Script =
                @"Insert Into tb_Pedido
             (nm_cliente, ds_cpf, dt_venda) 
             Values (nm_cliente, ds_cpf, dt_venda) ";
            List<MySqlParameter> Parms = new List<MySqlParameter>();
            Parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            Parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            Parms.Add(new MySqlParameter("dt_venda", dto.Data));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(Script, Parms); 
             
        } 
        public List<PedidoView> Consultar(string cliente)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_cliente like @nm_cliente";
            List<MySqlParameter> Parms = new List<MySqlParameter>();
            Parms.Add(new MySqlParameter("nm_cliente" + cliente + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, Parms);
            List<PedidoView> lista = new List<PedidoView>();
            while (reader.Read())
            {
                PedidoView dto = new PedidoView();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.QtdItens = reader.GetInt32("qtd_itens");
                dto.Data = reader.GetDateTime("dt_venda");
                dto.Total = reader.GetDecimal("vl_total");
                lista.Add(dto);
            }
            reader.Close();
            return lista;  
        }


      
        }
    }
}
