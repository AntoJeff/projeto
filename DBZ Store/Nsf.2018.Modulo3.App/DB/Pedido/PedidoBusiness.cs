﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO Pedido, List<PedidoDTO> produtos)
        {
            PedidoDatabase PedidoDatabase = new PedidoDatabase();
            int Idpedido = PedidoDatabase.Salvar(Pedido);

            PedidoItemBusiness ItemBusiness = new PedidoItemBusiness();

            foreach (PedidoDTO item in produtos)
            {
                PedidoItemDTO ItemDto = new PedidoItemDTO();
                ItemDto.IdPedido = Idpedido;
                ItemDto.IdProduto = item.Id;
                ItemBusiness.Salvar(ItemDto);


            }


            return Idpedido;


        }

        public List<PedidoView> Consultar(string Cliente)
        {

            PedidoDatabase PedidoDatabase = new PedidoDatabase();
            return PedidoDatabase.Consultar(Cliente);
        }
    }
}
