﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoItemDatabase
    { 
        public int Salvar(PedidoItemDTO dto)
        {
            string Script = @"INSERT INTO tb_pedido_item (id_produto, id_pedido) 
                        VALUES (@id_produto, @id_pedido)";
            List <MySqlParameter> Parms = new List<MySqlParameter>();
            Parms.Add(new MySqlParameter("id_produto",dto.IdProduto));
            Parms.Add(new MySqlParameter("id_pedido", dto.IdPedido));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(Script, Parms); 
        }
    }
}
